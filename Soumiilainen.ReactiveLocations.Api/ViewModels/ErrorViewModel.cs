﻿namespace Soumiilainen.ReactiveLocations.Api.ViewModels
{
    public class ErrorViewModel
    {
        public ErrorViewModel(string message)
        {
            Message = message;
        }

        public string Message { get;set; }
    }
}
