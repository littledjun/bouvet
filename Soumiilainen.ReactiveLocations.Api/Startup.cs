﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Middleware;
using Soumiilainen.ReactiveLocations.Percistence;
using Swashbuckle.AspNetCore.Swagger;

namespace Soumiilainen.ReactiveLocations.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            ConfigureLogging(services);
            ConfigureStorage(services);
            //services.RegisterDummyRepositories("ThisIsWe.Common.Entities");


            //Swagger
            services.AddSwaggerGen(config =>
            {
                config.DescribeAllEnumsAsStrings();
                config.SwaggerDoc("v1", new Info { Title = "Reactive Locations API", Version = "v1" });
                config.AddSecurityDefinition("X-Api-Key", new ApiKeyScheme()
                {
                    Description = "Authorization",
                    Name = "X-Api-Key",
                    In = "header"
                });
                config.DocumentFilter<ApiKeyDocumentFilter>();
            });


            // Services
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();

            //Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Reactive Locations API");
            });


            //Custom Middlewares
            app.UseDefaultMiddlewares();
            app.UseMvc();
        }

        private void ConfigureStorage(IServiceCollection services)
        {
            services.RegisterStorage(new ConnectionSettings()
            {
                ConnectionString = Configuration.GetConnectionString("Default"),
                DatabaseName = Configuration.GetSection("DatabaseNames")["Default"]
            });
            services.RegisterMongoRepositories("Soumiilainen.ReactiveLocations.Common.Entities");
        }

        private void ConfigureLogging(IServiceCollection services)
        {
            
        }
    }
}
