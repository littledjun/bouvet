﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Soumiilainen.ReactiveLocations.Api
{
    public static  class Extensions
    {
    public static string ToGeoString(this double value)
    {
        return value.ToString("R",CultureInfo.InvariantCulture);
    }
}
}
