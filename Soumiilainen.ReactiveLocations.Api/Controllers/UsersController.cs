﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;

namespace Soumiilainen.ReactiveLocations.Api.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IRepository<User> _userRepository;

        public UsersController(ILogger<UsersController> logger, IRepository<User> userRepository) : base(logger)
        {
            _userRepository = userRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateUserRequest request)
        {
            var user = new User()
            {
                Name = request.Name
            };
            await _userRepository.AddAsync(user);
            return Ok(user);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById([FromRoute]string id)
        {
            var result = await _userRepository.GetByIdAsync(id);
            return Ok(result);
        }
    }
}