﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GeoCoordinatePortable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Driver.GeoJsonObjectModel;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;
using Soumiilainen.ReactiveLocations.Percistence;

namespace Soumiilainen.ReactiveLocations.Api.Controllers
{
    public class LocationsController : BaseController
    {
        private readonly IRepository<ReactiveLocation> _locationsRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Category> _categoryRepository;

        public LocationsController(ILogger<LocationsController> logger, IRepository<ReactiveLocation> locationsRepository, IRepository<User> userRepository, IRepository<Category> categoryRepository) : base(logger)
        {
            _locationsRepository = locationsRepository;
            _userRepository = userRepository;
            _categoryRepository = categoryRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateLocationRequest request)
        {
            var location = new ReactiveLocation()
            {
                Coordinates = request.Coordinates,
                Notes = request.Notes,
                Owner = await _userRepository.GetByIdAsync(request.OwnerId),
                GeoJson = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(request.Coordinates.Longitude, request.Coordinates.Latitude)),
                Categories = request.CategoryIds.Any() ? await _categoryRepository.GetAsync(x => request.CategoryIds.Contains(x.EntityId)) : new List<Category>()

            };
            await _locationsRepository.AddAsync(location);
            return Ok(location);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _locationsRepository.GetAsync(x => true);
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById([FromRoute]string id)
        {
            var result = await _locationsRepository.GetByIdAsync(id);
            if (result == null)
            {
                return NotFound();

            }
            return Ok(result);
        }

        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> DeleteById([FromBody]DeleteLocationRequest request)
        {
            var location = await _locationsRepository.GetByIdAsync(request.LocationId);
            if (location == null)
            {
                return NotFound();
            }
            if (location.Owner?.EntityId == request.OwnerId)
            {
                await _locationsRepository.RemoveAsync(request.LocationId);
                return Ok();
            }

            return StatusCode((int)HttpStatusCode.Forbidden);
        }

        [HttpPost]
        [Route("nearby")]
        public async Task<IActionResult> GetNearbyLocations([FromBody] NearbyLocationsRequest request)
        {
            var mongoRepo = _locationsRepository as MongoRepository<ReactiveLocation>;
            var result = await mongoRepo.FindNear(request.Latitude, request.Longitude, request.MaxDistance);
            //return Ok(result);
            //Rework to use DB Geo
            GeoCoordinate cords = new GeoCoordinate(request.Latitude, request.Longitude);
            var nearbyLocations = result.Select(x =>
                {
                    var nearbyLocation = new NearbyLocation()
                    {
                        Coordinates = x.Coordinates,
                        EntityId = x.EntityId,
                        CreatedAtUtc = x.CreatedAtUtc,
                        Notes = x.Notes,
                        Owner = x.Owner,
                        GeoJson = x.GeoJson,
                        Categories = x.Categories,
                        Distance = cords.GetDistanceTo(new GeoCoordinate(x.Coordinates.Latitude, x.Coordinates.Longitude))
                    };

                    return nearbyLocation;
                }
            );
            return Ok(new
            {
                StartingPoint = new Coordinates(request.Longitude, request.Latitude),
                MaxDistance = request.MaxDistance,
                NearbyLocations = nearbyLocations
            });
        }

        [HttpGet]
        [Route("{categoryId}")]
        public async Task<IActionResult> GetByCategoryId([FromRoute]string categoryId)
        {
            var result = await _locationsRepository.GetAsync(x => x.Categories.Any(y => y.EntityId.Equals(categoryId)));
            return Ok(result);
        }

        //[HttpPost]
        //[Route("clean")]
        //public async Task<IActionResult> CleanLocations()
        //{
        //    var locations =
        //        await _locationsRepository.GetAsync(x => x.CreatedAtUtc < DateTime.Now.Subtract(TimeSpan.FromDays(10)));
        //    var tasks = locations.Select(async x => await _locationsRepository.RemoveAsync(x.EntityId));
        //    await Task.WhenAll(tasks);
        //    return Ok();
        //}
    }

}

public class CreateUserRequest
{
    public string Name { get; set; }
}

public class CreateLocationRequest
{
    public string OwnerId { get; set; }
    public Coordinates Coordinates { get; set; }
    public string Notes { get; set; }
    public List<string> CategoryIds { get; set; } = new List<string>();
}
