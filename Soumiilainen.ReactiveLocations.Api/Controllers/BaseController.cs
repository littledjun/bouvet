﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;

namespace Soumiilainen.ReactiveLocations.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public abstract class BaseController : ControllerBase
    {
        protected readonly ILogger<BaseController> _logger;
        protected BaseController(ILogger<BaseController> logger)
        {
            _logger = logger;
        }
    }

    public abstract class CrudController<T> : BaseController
    {
        private readonly IRepository<T> _repository;

        protected CrudController(ILogger<CrudController<T>> logger, IRepository<T> repository) : base(logger)
        {
            _repository = repository;
        }

        [HttpGet]
        public virtual async Task<IActionResult> GetAll()
        {
            var result = await _repository.GetAsync(x => true);
            return Ok(result);
        }
    }
}
