﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;

namespace Soumiilainen.ReactiveLocations.Api.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly IRepository<Category> _categoryRepository;

        public CategoryController(IRepository<Category> categoryRepository, ILogger<CategoryController> logger) : base(logger)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateCategoryRequest request)
        {

            var category = new Category() { Name = request.Name};
            if (!string.IsNullOrEmpty(request.EntityId))
            {
                category.EntityId = request.EntityId;
            }
            await _categoryRepository.AddAsync(category);
            return Ok(category);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _categoryRepository.GetAsync(x => true);
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById([FromRoute]string id)
        {
            var result = await _categoryRepository.GetByIdAsync(id);
            return Ok(result);
        }
    }

    public class CreateCategoryRequest
    {
        public string Name { get; set; }
        public string EntityId { get; set; }
    }
}