﻿namespace Soumiilainen.ReactiveLocations.Api.Controllers
{
    public class NearbyLocationsRequest
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double MaxDistance { get; set; } = 1000;
    }

    public class DeleteLocationRequest
    {
        public string OwnerId { get; set; }
        public string LocationId { get; set; }
    }
}