﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Soumiilainen.ReactiveLocations.Abstractions.Repository
{
    public interface IRepository<TEntity>
    {
        Task AddAsync(TEntity entity);
        Task AddAsync(IEnumerable<TEntity> entities);
        Task RemoveAsync(string entityId);
        Task UpdateAsync(IEnumerable<TEntity> entity);
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> GetByIdAsync(string id);
        Task<IEnumerable<TEntity>> QueryAsync(FilterDefinition<TEntity> query);
    }
}
