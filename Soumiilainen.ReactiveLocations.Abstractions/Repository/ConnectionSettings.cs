﻿namespace Soumiilainen.ReactiveLocations.Abstractions.Repository
{
    public class ConnectionSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
