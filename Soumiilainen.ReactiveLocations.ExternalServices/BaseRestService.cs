﻿using RestSharp;

namespace Soumiilainen.ReactiveLocations.ExternalServices
{
    public abstract class BaseRestService
    {
        protected IRestClient _client;
        protected BaseRestService(string baseUrl)
        {
            _client = new RestClient(baseUrl);
        }


        protected IRestResponse<T> Get<T>(string resource, params Parameter[] parameters) where T : new()
        {
            IRestRequest request = new RestRequest
            {
                Resource = resource
            };
            if (parameters != null)
            {
                request.Parameters.AddRange(parameters);

            }

            return _client.Get<T>(request);
        }
    }
}
