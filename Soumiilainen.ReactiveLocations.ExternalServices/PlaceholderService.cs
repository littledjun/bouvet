﻿using System.Collections.Generic;
using RestSharp;

namespace Soumiilainen.ReactiveLocations.ExternalServices
{
    public class PlaceholderService : BaseRestService, IPlaceholderService
    {

        public PlaceholderService() : base("https://jsonplaceholder.typicode.com/")
        {
            
        }
        public PlaceholderService(string baseUrl) : base(baseUrl)
        {
        }

        public IEnumerable<Post> GetPosts()
        {
            var result = Get<List<Post>>("posts", new Parameter("userId", 2, ParameterType.QueryString));
            return result.Data;
        }
    }

    public interface IPlaceholderService
    {
        IEnumerable<Post> GetPosts();
    }
    public class Post
    {
        public Post()
        {
            
        }
        public int UserId { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
