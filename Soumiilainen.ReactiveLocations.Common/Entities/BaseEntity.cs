﻿using System;
using System.Collections.Generic;
using System.Globalization;
using MongoDB.Driver.GeoJsonObjectModel;

namespace Soumiilainen.ReactiveLocations.Common.Entities
{
    public abstract class BaseEntity
    {
        public string EntityId { get; set; } = Guid.NewGuid().ToString();
    }



    [MongoEntity]
    public class ReactiveLocation : BaseEntity
    {
        public User Owner { get; set; }
        public Coordinates Coordinates { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> GeoJson { get; set; }
        public string Notes { get; set; }
        public DateTime CreatedAtUtc { get; set; } = DateTime.UtcNow;

        [MongoIndex(1, false)]
        public IEnumerable<Category> Categories { get; set; }

        public string GetGeoString()
        {
            return
                $"{Coordinates.Latitude.ToString(CultureInfo.InvariantCulture)},{Coordinates.Longitude.ToString(CultureInfo.InvariantCulture)}";
        }
        public string GLink => $"http://www.google.com/maps/place/{GetGeoString()}";
    }

    public class NearbyLocation : ReactiveLocation
    {
        public double Distance { get; set; }
    }
    public sealed class MongoEntityAttribute : Attribute
    {
    }

    public sealed class MongoIndexAttribute : Attribute
    {
        public int Direction { get; }
        public bool IsUnique { get; private set; }
        public int Ttl { get; private set; }


        public MongoIndexAttribute(int direction = 1, bool isUnique = false, int ttl = 0)
        {
            Direction = direction;
            IsUnique = isUnique;
            Ttl = ttl;
        }
    }

    public class Coordinates
    {
        public Coordinates(double longitude, double latitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
