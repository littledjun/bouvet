﻿namespace Soumiilainen.ReactiveLocations.Common.Entities
{
    [MongoEntity]
    public class User : BaseEntity
    {
        public string Name { get; set; }
    }
}
