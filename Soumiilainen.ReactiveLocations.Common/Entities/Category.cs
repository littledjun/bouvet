﻿namespace Soumiilainen.ReactiveLocations.Common.Entities
{
    [MongoEntity]
    public class Category : BaseEntity
    {
        public string Name { get; set; }
    }
}
