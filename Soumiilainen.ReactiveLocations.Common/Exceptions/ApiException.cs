﻿using System;

namespace Soumiilainen.ReactiveLocations.Common.Exceptions
{
    public class ApiException : Exception
    {
        public ApiException(string message) : base(message)
        {
            
        }
    }
}
