﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Soumiilainen.ReactiveLocations.Common.Middleware
{
    public class ProcessingTime
    {
        private readonly RequestDelegate _next;
        private const string Key = "x-processing-time";
        public ProcessingTime(RequestDelegate next)

        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var sw = Stopwatch.StartNew();
            context.Response.OnStarting(state => {
                sw.Stop();
                var httpContext = (HttpContext)state;
                httpContext.Response.Headers.Add(Key, new[] { sw.ElapsedMilliseconds.ToString() });
                return Task.FromResult(0);
            }, context);

            await _next(context);
        }
    }
}
