﻿using Microsoft.AspNetCore.Builder;

namespace Soumiilainen.ReactiveLocations.Common.Middleware
{
    public static class Infrastructure
    {
        public static IApplicationBuilder UseProcessingTime(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ProcessingTime>();
        }

        public static IApplicationBuilder UseApiKey(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ApiKeyValidator>();
        }

        /// <summary>
        /// Adds Default middlewares like Apikey, CorrelationId, processingtime
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseDefaultMiddlewares(this IApplicationBuilder app)
        {
            app.UseMiddleware<ApiKeyValidator>();
            app.UseMiddleware<CorrelationId>();
            app.UseMiddleware<ProcessingTime>();
            return app;
        }
    }
}
