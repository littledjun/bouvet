﻿using System;

namespace Soumiilainen.ReactiveLocations.Common.Middleware
{
    public class ApiKeyErrorResponse
    {
        public string Message { get; set; } = "Not Allowed";
        public DateTime Timestamp { get; } = DateTime.UtcNow;
    }
}