﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Soumiilainen.ReactiveLocations.Common.Middleware
{
    public class ApiKeyValidator
    {
        private RequestDelegate _next;
        private const string Key = "X-Api-Key";
        private const string Secret = "sUPER_DUPER_SECREt";

        public ApiKeyValidator(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILogger<ApiKeyValidator> logger)
        {
            //TODO why value not used ??
            //context.Request.Headers.TryGetValue(Key, out var value);
            if (!Authenticate(context))
            {
                var msg = JsonConvert.SerializeObject(new ApiKeyErrorResponse());
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                logger.LogWarning("Unauthorized attempt: " + context.Request.Body);
                await context.Response.WriteAsync(msg);
            }
            else
            {
                await _next(context);
            }
        }

        private bool Authenticate(HttpContext context)
        {
            var headerExists = context.Request.Headers.TryGetValue(Key, out var value);
            if (headerExists && value[0].Equals(Secret))
                return true;
            return false;
        }
    }
}
