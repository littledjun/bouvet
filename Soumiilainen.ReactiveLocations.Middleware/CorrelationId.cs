﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Soumiilainen.ReactiveLocations.Common.Middleware
{
    public class CorrelationId : DelegatingHandler
    {
        private RequestDelegate _next;
        private const string Key = "x-correlation-id";

        public CorrelationId(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var correlationId = Guid.NewGuid().ToString();

            context.Request.Headers.Add(Key, correlationId);
            await _next(context);
        }
    }
}
