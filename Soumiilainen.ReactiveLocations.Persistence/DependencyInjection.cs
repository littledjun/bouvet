﻿using System;
using System.Linq;
using System.Security.Authentication;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;
using ConnectionSettings = Soumiilainen.ReactiveLocations.Abstractions.Repository.ConnectionSettings;

namespace Soumiilainen.ReactiveLocations.Percistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection RegisterStorage(this IServiceCollection services,
            ConnectionSettings connectionSettings, bool useDummy = false)
        {
            if (useDummy)
            {
                return services.RegisterDummyRepositories("ThisIsWe.Common.Entities");

            }
            MongoClientSettings settings =
                MongoClientSettings.FromUrl(new MongoUrl(connectionSettings.ConnectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            var mongoClient = new MongoClient(settings);
            var db = mongoClient.GetDatabase(connectionSettings.DatabaseName);
            services.AddSingleton(typeof(IMongoDatabase), db);

            return services;
        }

        public static IServiceCollection RegisterMongoRepositories(this IServiceCollection services, string entitiesNamespace)
        {
            IgnoreIdConvention();
            return services.RegisterTypes(typeof(IRepository<>), typeof(MongoRepository<>), entitiesNamespace, ServiceLifetime.Transient);
        }


        private static IServiceCollection RegisterTypes(this IServiceCollection services, Type interfaceType, Type implementationType, string entitiesNamespace, ServiceLifetime scope)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var classes = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(t => t.GetCustomAttributes(typeof(MongoEntityAttribute), true).Length > 0).ToList();
                    //t.Namespace == entitiesNamespace 
                    //        && t.IsClass 
                            //&& !t.Name.ToLower().Contains("base")).ToList();

            classes.ForEach(x =>
                {
                    var genericRepo = implementationType;
                    var concreteType = genericRepo.MakeGenericType(x);
                    switch (scope)
                    {
                        case ServiceLifetime.Scoped:
                            services.AddScoped(interfaceType.MakeGenericType(x), concreteType);
                            break;
                        case ServiceLifetime.Transient:
                            services.AddTransient(interfaceType.MakeGenericType(x), concreteType);
                            break;
                        case ServiceLifetime.Singleton:
                            services.AddSingleton(interfaceType.MakeGenericType(x), concreteType);
                            break;
                    }
                }
            );

            return services;
        }
        private static void IgnoreIdConvention()
        {
            var pack = new ConventionPack
            {
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("Ignore ID", pack, t => true);
        }

        public static IServiceCollection RegisterDummyRepositories(this IServiceCollection services, string entitiesNamespace)
        {
            return services.RegisterTypes(typeof(IRepository<>), typeof(InMemoryRepository<>), entitiesNamespace, ServiceLifetime.Singleton);
        }
    }
}
