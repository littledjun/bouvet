﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;
using Soumiilainen.ReactiveLocations.Events.Events;

namespace Soumiilainen.ReactiveLocations.Percistence
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IEventPublisher _eventPublisher;
        private readonly ILogger<InMemoryRepository<T>> _logger;
        private List<T> _repo = new List<T>();

        public InMemoryRepository(IEventPublisher eventPublisher, ILogger<InMemoryRepository<T>> logger)
        {
            _eventPublisher = eventPublisher;
            _logger = logger;
        }
        public async Task AddAsync(T entity)
        {
            _repo.Add(entity);
            await Task.CompletedTask;
        }

        public async Task AddAsync(IEnumerable<T> entities)
        {
            _repo.AddRange(entities);
            await Task.CompletedTask;

        }

        public Task RemoveAsync(string entityId)
        {
            return Task.FromResult(_repo.RemoveAll(x => x.EntityId == entityId));
        }

        public async Task RemoveAsync(T entity)
        {
            _repo.Remove(entity);
            await Task.CompletedTask;

        }

        public Task UpdateAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                if (_repo.Exists(x => x.EntityId == entity.EntityId))
                {
                    var index = _repo.FindIndex(x => x.EntityId == entity.EntityId);
                    _repo[index] = entity;
                }
            }

            return Task.CompletedTask;
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression)
        {
            return await Task.FromResult(_repo.Where(expression.Compile()));
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return await Task.FromResult(_repo.FirstOrDefault(x => x.EntityId.Equals(id)));
        }

        public Task<IEnumerable<T>> QueryAsync(FilterDefinition<T> query)
        {
            throw new NotImplementedException();
        }
    }
}