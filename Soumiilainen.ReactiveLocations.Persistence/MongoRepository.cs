﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using Soumiilainen.ReactiveLocations.Abstractions.Repository;
using Soumiilainen.ReactiveLocations.Common.Entities;

namespace Soumiilainen.ReactiveLocations.Percistence
{
    public class MongoRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IMongoCollection<TEntity> _collection;
        public string CollectionName { get; set; } = typeof(TEntity).Name.ToLowerInvariant(); // Default to Entity name

        public MongoRepository(IMongoDatabase mongoDb)
        {

            _collection = mongoDb.GetCollection<TEntity>(CollectionName);
            EnsureIndex();
        }

        public async Task AddAsync(TEntity entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task AddAsync(IEnumerable<TEntity> entities)
        {
            await _collection.InsertManyAsync(entities);
        }

        public async Task RemoveAsync(string entityId)
        {
            await _collection.FindOneAndDeleteAsync(x => x.EntityId == entityId);
        }

        private FilterDefinition<TEntity> FilterById(string id)
        {
            //if (!ObjectId.TryParse(id, out var objectId))
            //{
            //    throw new Exception();
            //}
            return Builders<TEntity>.Filter.Eq(x => x.EntityId, id);
        }

        public Task UpdateAsync(IEnumerable<TEntity> entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> expression)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression)
        {

            var result = await _collection.FindAsync(expression);
            return await result.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(string id)
        {
            var result = await _collection.FindAsync(FilterById(id));
            return await result.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> QueryAsync(FilterDefinition<TEntity> query)
        {
            var result = await _collection.FindAsync(query);
            return await result.ToListAsync();
        }

        private bool EnsureIndex()
        {
            var type = typeof(TEntity);
            var propertyInfos = type.GetPropertiesWithAttribute(typeof(MongoIndexAttribute), true);
            foreach (var propertyInfo in propertyInfos)
            {
                var indexAttribute = propertyInfo.GetCustomAttribute(typeof(MongoIndexAttribute), true) as MongoIndexAttribute;
                var collectionName = propertyInfo.Name;

            }
            return true;
        }
    }

    public static class MongoRepositoryExtensions
    {
        public static async Task<IEnumerable<ReactiveLocation>> FindNear(this MongoRepository<ReactiveLocation> repo, double lat,
            double lang, double maxDistance, double minDistance = 0)
        {
            var point = GeoJson.Point(GeoJson.Geographic(lang, lat));
            var query = Builders<ReactiveLocation>.Filter.Near("GeoJson", point, maxDistance, minDistance);
            var result = await repo.QueryAsync(query);
            return result;
        }
    }


    public static class GenericExtensions
    {

        public static IEnumerable<PropertyInfo> GetPropertiesWithAttribute(this Type obj, Type attributeType, bool inherit = false)
        {
            var propertyInfos = obj.GetProperties().Where(x => x.GetCustomAttributes(attributeType, inherit).Length > 0);
            return propertyInfos;
        }
    }
}