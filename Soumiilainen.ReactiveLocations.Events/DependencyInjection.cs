﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Soumiilainen.ReactiveLocations.Events.Events;

namespace Soumiilainen.ReactiveLocations.Events
{
    public static class DependencyInjection
    {
        public static IServiceCollection RegisterEventConsumers(this IServiceCollection services)
        {
            var genericTypeDef = typeof(IEventConsumer<>);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var concreteConsumers = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(type => type.IsClass && type.GetInterfaces()
                                   .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == genericTypeDef))
                .ToList();

            concreteConsumers.ForEach(implementationType =>
            {
                var eventConsumers = implementationType.GetInterfaces()
                    .Where(x => x.IsGenericType && x.GetGenericTypeDefinition() == genericTypeDef).ToList();
                eventConsumers.ForEach(x =>
                {
                    var eventType = x.GetGenericArguments().FirstOrDefault();
                    var serviceType = genericTypeDef.MakeGenericType(eventType);
                    services.AddTransient(serviceType, implementationType);
                });
            });
            return services;
        }
    }
}
