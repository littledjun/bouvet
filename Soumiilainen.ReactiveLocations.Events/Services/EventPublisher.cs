﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Soumiilainen.ReactiveLocations.Events.Events;

namespace Soumiilainen.ReactiveLocations.Events.Services
{
    public class EventPublisher : IEventPublisher
    {
        private readonly IServiceProvider _serviceProvider;

        public EventPublisher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Publish<T>(T @event)
        {
            var handlers = GetEventConsumers<IEventConsumer<T>>().ToList();
            handlers.ForEach(handler =>
            {
                handler.Handle(@event);
            });
        }

        private IEnumerable<T> GetEventConsumers<T>()
        {

            var services = _serviceProvider.GetServices<T>();
            return services;
        }
    }
}
