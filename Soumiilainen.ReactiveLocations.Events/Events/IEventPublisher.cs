﻿namespace Soumiilainen.ReactiveLocations.Events.Events
{
    public interface IEventPublisher
    {
        void Publish<T>(T @event);
    }
}
