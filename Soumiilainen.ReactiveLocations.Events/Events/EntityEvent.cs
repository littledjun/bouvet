﻿namespace Soumiilainen.ReactiveLocations.Events.Events
{
    public class EntityCreatedEvent<T> : BaseEvent<T>
    {
        public EntityCreatedEvent(T data) : base(data)
        {
        }
    }

    public class EntityUpdatedEvent<T> : BaseEvent<T>
    {
        public EntityUpdatedEvent(T data) : base(data)
        {
        }
    }

    public class EntityRemovedEvent<T> : BaseEvent<T>
    {
        public EntityRemovedEvent(T data) : base(data)
        {
        }
    }
}