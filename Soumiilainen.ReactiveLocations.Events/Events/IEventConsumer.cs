﻿
namespace Soumiilainen.ReactiveLocations.Events.Events
{
    public interface IEventConsumer<T>
    {
        void Handle(T @event);
    }
}
