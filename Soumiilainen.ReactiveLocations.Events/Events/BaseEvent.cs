﻿using System;

namespace Soumiilainen.ReactiveLocations.Events.Events
{
    public abstract class BaseEvent<T>
    {
        public T Data { get; }
        public Guid EventId { get; }
        public DateTime Timestamp { get; }
        protected BaseEvent(T data)
        {
            Data = data;
            EventId = Guid.NewGuid();
            Timestamp = DateTime.UtcNow;
        }
    }
}
